package finest.project.medo;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Dashboard extends AppCompatActivity {

    Button dashBtn1,dashRegister,dashBtn3;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);

        dashBtn1 = findViewById(R.id.dashBtn1);
        dashBtn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Dashboard.this,Booking_appointment.class);
                startActivity(intent);
            }
        });

        dashRegister = findViewById(R.id.dashRegister);
        dashRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Dashboard.this,user_register.class);
                startActivity(intent);
            }
        });
        dashBtn3 = findViewById(R.id.dashBtn3);
        dashBtn3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Dashboard.this,login.class);
                startActivity(intent);
            }
        });
    }
}