package finest.project.medo;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class signup extends AppCompatActivity {

    EditText Username, Email, Password, Confirmpassword;
    Button button;
    DatabaseHelper db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        Username =findViewById(R.id.Username);
        Email = findViewById(R.id.Email);
        Password =findViewById(R.id.Password);
        Confirmpassword = findViewById(R.id.Confirmpassword);
        button = findViewById(R.id.signBtn1);
        db = new DatabaseHelper(this);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String user = Username.getText().toString();
                String email = Email.getText().toString();
                String password = Password.getText().toString();
                String confirmpassword= Confirmpassword.getText().toString();

                if (user.isEmpty()) {
                    Username.setError("Username is required");
                    Username.requestFocus();
                    return;
                }

                if (email.isEmpty()) {
                    Email.setError("Email is required");
                    Email.requestFocus();
                    return;
                }

                if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
                    Email.setError("Please enter a valid email");
                    Email.requestFocus();
                    return;
                }

                if (password.isEmpty()) {
                    Password.setError("Password is required");
                    Password.requestFocus();
                    return;
                }

                if (password.length() < 6) {
                    Password.setError("Minimum length of password should be 6");
                    Password.requestFocus();
                    return;
                }

                if (!confirmpassword.equals(password)) {
                    Confirmpassword.setError("Password does not match");
                    Confirmpassword.requestFocus();
                    return;
                }

                // check if user already exists
                if (db.checkUser(email)) {
                    Toast.makeText(signup.this, "Email already exists", Toast.LENGTH_SHORT).show();
                    return;
                }


                if(user.equals("")|| email.equals("") || password.equals("") || confirmpassword.equals("")){
                    Toast.makeText(signup.this,"Please Enter all the fields", Toast.LENGTH_SHORT).show();
                }
                else{
                    Boolean checkuser = db.checkusername(user);
                    if(checkuser == false){
                        Boolean insert = db.insertData(user, email, password, confirmpassword);
                        if(insert==true){
                            Toast.makeText(signup.this,"Registered Successfully", Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(getApplicationContext(), Dashboard.class);
                            startActivity(intent);
                        }else{
                            Toast.makeText(signup.this,"Registration failed", Toast.LENGTH_SHORT).show();
                        }

                    }else{
                        Toast.makeText(signup.this, "users already exists",Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }
}