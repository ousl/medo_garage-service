package finest.project.medo;
import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

import java.util.ArrayList;

public class DatabaseHelper extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "FitLifeDatabase";
    private static final int DB_VERSION = 1;
    private static final String SERVICE_TABLE = "serviceTable";
    private static final String ID_COL = "id";
    private static final String B_NAME="BusinessName";
    private static final String S_NAME="ServiceName";
    private static final String EMAIL="Email";
    private static final String B_OVERVIEW="Overview";
    private static final String CAPTION="Caption";
    private Context context ;

    public DatabaseHelper(Context context){

        super(context, DATABASE_NAME, null, DB_VERSION);
        this.context = context;

    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table users (Username TEXT primary key, Email TEXT, Password TEXT, Confirmpassword TEXT)");
        String createTableQuery = "CREATE TABLE " + SERVICE_TABLE + "("
                + ID_COL + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + B_NAME + " TEXT,"
                + S_NAME + " TEXT,"
                + EMAIL + " TEXT,"
                + B_OVERVIEW + " TEXT,"
                + CAPTION + " TEXT)";


        // at last we are calling a exec sql
        // method to execute above sql query
        db.execSQL(createTableQuery);

    }
    public void addNewService(String businessName, String serviceName, String email, String overview, String caption) {


        SQLiteDatabase db = this.getWritableDatabase();


        ContentValues values = new ContentValues();


        values.put(B_NAME, businessName);
        values.put(S_NAME, serviceName);
        values.put(EMAIL, email);
        values.put(B_OVERVIEW, overview);
        values.put(CAPTION, caption);


        db.insert(SERVICE_TABLE, null, values);

        db.close();
    }
    // we have created a new method for reading all the courses.



    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        db.execSQL("drop Table if exists users");
        db.execSQL("DROP TABLE IF EXISTS " + SERVICE_TABLE);
        onCreate(db);
    }

    public boolean insertData(String Username, String Email, String Password, String Confirmpassword) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentvalues = new ContentValues();
        contentvalues.put("Username", Username);
        contentvalues.put("Email", Email);
        contentvalues.put("Password", Password);
        contentvalues.put("Confirmpassword", Confirmpassword);

        long result = db.insert("users", null, contentvalues);
        if (result == -1) {
            return false;
        } else {
            return true;
        }
    }


    public Boolean checkusername(String Username){
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery("Select * from users where Username = ?",new String[]{Username});
        if (cursor.getCount()>0)
            return  true;
        else
            return false;

    }

    public boolean checkusernamepassword(String Username, String Password){
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery("Select * from users where Username = ? and Password = ? ", new String[]{Username,Password});
        if (cursor.getCount()>0)
            return  true;
        else
            return false;
    }

    public boolean checkUser(String email) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM users WHERE email=?", new String[]{email});
        boolean exists = cursor.getCount() > 0;
        cursor.close();
        return exists;
    }


}