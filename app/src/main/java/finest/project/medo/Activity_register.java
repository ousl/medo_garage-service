package finest.project.medo;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Activity_register extends AppCompatActivity {
    private EditText businessNameEDT,serviceNameEDT,emailEDT,businessOverviewEDT,captionEDT;
    private Button addService;
    private DatabaseHelper db;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        businessNameEDT = findViewById(R.id.businessName);
        serviceNameEDT = findViewById(R.id.serviceName);
        emailEDT = findViewById(R.id.email);
        businessOverviewEDT = findViewById(R.id.businessOverview);
        captionEDT = findViewById(R.id.caption);
        addService = findViewById(R.id.addService);

        db = new DatabaseHelper(Activity_register.this);

        addService.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Toast.makeText(Activity_register.this, "Clicked.", Toast.LENGTH_SHORT).show();
//                String businessName = businessNameEDT.getText().toString();
//                String serviceName = serviceNameEDT.getText().toString();
//                String email = emailEDT.getText().toString();
//                String businessOverview = businessOverviewEDT.getText().toString();
//                String caption = captionEDT.getText().toString();
//
//                if (businessName.isEmpty() && serviceName.isEmpty() && email.isEmpty() && businessOverview.isEmpty() && caption.isEmpty()) {
//                    Toast.makeText(Activity_register.this, "please fill all the Data..", Toast.LENGTH_SHORT).show();
//                    return;
//                }
//                db.addNewService(businessName, serviceName, email, businessOverview,caption);
//                // after adding the data we are displaying a toast message.
//                Toast.makeText(Activity_register.this, "Service has been added.", Toast.LENGTH_SHORT).show();
//                businessNameEDT.setText("");
//                serviceNameEDT.setText("");
//                emailEDT.setText("");
//                businessOverviewEDT.setText("");
//                captionEDT.setText("");

            }
        });
        Button btn = (Button)findViewById(R.id.location);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Activity_register.this, MapsActivity.class));
            }
        });
    }
}